# PHPUnit 开发设置

本文主要针对 PHPStorm 这个开发 IDE。

目前不推荐使用 `composer global require phpunit/phpunit` 或 在项目中 `composer require phpunit/phpunit` 做法。

前者无法被 PHPStorm 识别，后者，会在项目引入过多的包。

## 推荐做法

直接从 [PHPUnit 8 下载](https://phar.phpunit.de/phpunit-8.phar) 最新版的 phpunit 的 phar 包，并保存到本地任意目录，比如 `/Users/username/phar` 。

打开 PHPStorm 的设置，找到：

**Languages & Frameworks -> PHP**

1. 设置 **PHP language level** 为你本机 PHP 的 language level
2. 设置 **CLI Interpreter**，指向你本机的 PHP 可执行环境。
3. 在 **Include Path** 添加 `/Users/username/phar` 目录（也可以只添加 `/Users/username/phar/phpunit.phar`）

在 `tests` 目录下添加一个 `bootstrap.php` 文件，内容如下：

普通开发者：

```php
<?php

require '../bootstrap.php';

\Ke\Cli\Console::getConsole();
```

kephp 开发者：

```php
<?php

require __DIR__ . '/../vendor/autoload.php';

/** @var \Ke\App $APP */
global $app;

try {
	$app = new \Ke\App(__DIR__);
	$app->getLoader()->loadHelper('string');
} catch (Throwable $throw) {
	print $throw->getMessage();
}
```

**Languages & Frameworks -> PHP -> Test Frameworks**

1. 在 **PHPUnit library** 的 Section，勾选 `Path to phpunit.phar`
2. 在 **Path to phpunit.phar** 填写你本地的 phar 路径，如：`/Users/username/phar/phpunit.phar`
3. 在 **Test Runner** 的 Section ，勾选 `Default bootstrap file` ，并添加刚才添加的 `bootstrap.php` 文件。